package com.zy.blog.api;

import com.zy.blog.server.annotation.CoreMapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *  Spring Boot程序入口
 *
 *  @author zy
 *  @since 2020/4/14 22:40
 */
@SpringBootApplication(scanBasePackages={"com.zy.blog"})
@CoreMapperScan
public class BlogApplication {
    public static void main(String[] args) {
        SpringApplication.run(BlogApplication.class, args);
    }
}
