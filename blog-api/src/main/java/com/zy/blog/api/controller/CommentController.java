package com.zy.blog.api.controller;

import com.zy.blog.common.entity.CustomizedResponseEntity;
import com.zy.blog.bean.dto.CommentAddRequestDTO;
import com.zy.blog.bean.vo.ArticleVO;
import com.zy.blog.server.service.ICommentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 *  @author zy
 *  @since 2020/5/4 20:29
 *  @Description: 文章评论控制器
 */
@Api(tags = "文章评论接口")
@RestController
@RequestMapping("/api/blog//comment")
@Validated
public class CommentController {
    @Autowired
    private ICommentService commentService;

    @ApiOperation(value = "新增文章评论")
    @PostMapping("/add")
    public CustomizedResponseEntity<ArticleVO> getArticle(@ApiParam(value = "文章评论",required = true) @RequestBody @Valid CommentAddRequestDTO commentAddRequestDTO){
        return CustomizedResponseEntity.success(commentService.insertComment(commentAddRequestDTO));
    }

    @ApiOperation(value = "删除文章评论")
    @GetMapping("/delete")
    public CustomizedResponseEntity<String> login(@ApiParam(value = "评论id",required = true) @RequestParam("commentId") @NotNull(message="评论id不能为空") Integer commentId){
        commentService.deleteComment(commentId);
        return CustomizedResponseEntity.success();
    }
}
