package com.zy.blog.api.controller;

import com.zy.blog.common.entity.CustomizedResponseEntity;
import com.zy.blog.bean.vo.TagVO;
import com.zy.blog.server.service.ITagService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Author: zy
 * Description: 标签控制器
 * Date: 2020/4/22
 */
@Api(tags="标签接口")
@RestController
@RequestMapping("/api/blog/tag")
@Validated
public class TagController {
    @Autowired
    private ITagService tagService;

    @ApiOperation(value = "获取所有标签信息")
    @GetMapping("/list")
    public CustomizedResponseEntity<List<TagVO>> getTagList(){
        return CustomizedResponseEntity.success(tagService.getTagList());
    }
}
