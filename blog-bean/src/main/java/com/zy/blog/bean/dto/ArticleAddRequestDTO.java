package com.zy.blog.bean.dto;


import lombok.Data;

/**
 *  @author zy
 *  @since 2020/5/17 18:57
 *  @Description:新增文章前端要传的json格式
 */
@Data
public class ArticleAddRequestDTO extends ArticleRequestDTO {

}
