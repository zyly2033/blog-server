package com.zy.blog.bean.vo;


import com.zy.blog.bean.model.BlogArticle;
import com.zy.blog.bean.model.BlogTag;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 *  @author zy
 *  @since 2020/4/19 19:12
 *  @Description: 文章信息
 */
@Data
public class ArticleVO extends BlogArticle {
    @ApiModelProperty("文章标签")
    private List<BlogTag> tags;

    @ApiModelProperty("文章评论")
    private List<CommentVO> comments;
}
