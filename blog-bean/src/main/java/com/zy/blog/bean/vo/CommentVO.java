package com.zy.blog.bean.vo;


import com.zy.blog.bean.model.BlogComment;
import com.zy.blog.bean.model.BlogUser;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 *  @author zy
 *  @since 2020/4/25 19:57
 *  @Description: 评论信息
 */
@Data
public class CommentVO extends BlogComment {
    @ApiModelProperty("评论回复")
    List<ReplyVO> replies;

    @ApiModelProperty("评论人")
    BlogUser user;
}
