package com.zy.blog.bean.vo;


import com.zy.blog.bean.model.BlogUser;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 *  @author zy
 *  @since 2020/4/19 17:32
 *  @Description: 用户登录 返回用户信息
 */
@Data
public class LoginVO  extends BlogUser {
    @ApiModelProperty("token")
    private String token;
}
