package com.zy.blog.bean.vo;

import com.zy.blog.bean.model.BlogReply;
import com.zy.blog.bean.model.BlogUser;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Author: zy
 * Description: 回复人信息
 * Date: 2020/4/30
 */
@Data
public class ReplyVO extends BlogReply {
    @ApiModelProperty("回复人")
    BlogUser user;
}
