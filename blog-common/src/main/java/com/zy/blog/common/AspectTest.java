package com.zy.blog.common;

import com.zy.blog.common.annotation.Aspect;
import com.zy.blog.common.annotation.Before;
import com.zy.blog.common.annotation.Pointcut;
import org.springframework.stereotype.Component;


/**
 *  这是一个切面类 不可以使用@Configuration注解(会被动态代理)
 *
 *  @author zy
 *  @since 2020/6/21 10:12
 */
@Component
@Aspect
public class AspectTest {

    /**
     *  定义一个切点 目前只支持指定包路径
     */
    @Pointcut("com.zy.blog.server")
    public void servicePointCut(){

    }

    /**
     *  前置通知
     */
    @Before("servicePointCut()")
    public void testBefore(){
        System.out.println("before -----------------------,测试成功");
    }
}
