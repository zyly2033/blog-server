package com.zy.blog.common.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


/**
 *  后置通知注解类
 *
 *  @author  zy
 *  @since 2020/6/20 17:32
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface After {
    String value() default "";
}
