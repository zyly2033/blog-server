package com.zy.blog.common.annotation;


import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 *  环绕通知注解类
 *
 *  @author zy
 *  @since 2020/6/20 17:33
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Around {
    String value() default "";
}
