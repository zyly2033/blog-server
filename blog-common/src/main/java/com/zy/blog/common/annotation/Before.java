package com.zy.blog.common.annotation;


import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 *  前置通知注解类
 *
 *  @author zy
 *  @since 2020/6/20 17:31
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Before {
    String value() default "";
}
