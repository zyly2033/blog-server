package com.zy.blog.common.annotation;


import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 *  切点
 *
 *  @author zy
 *  @since 2020/6/21 17:54
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Pointcut {
    String value() default "";
}
