package com.zy.blog.common.converter;

import com.zy.blog.common.enums.IBaseEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.converter.ConverterFactory;

/**
 * Author: zy
 * Description: get方式请求时String->IbaseEnum类型转换器
 * Date: 2020/5/21
 */
@Slf4j
public class EnumConverterFactory implements ConverterFactory<String, IBaseEnum> {

    /**
     * @author: zy
     * @description: 获取String->targetType类型的转换器 用于支持自己IBaseEnum类型(Spring支持常规的枚举类型)
     * @date: 2020/5/21 10:17
     * @param targetType: 目标类型
     * @return Converter<String,T>:
     */
    @Override
    public <T extends IBaseEnum> Converter<String, T> getConverter(Class<T> targetType) {
        //获取给定类型对应的转换器
        return new StrToEnum(targetType);
    }

    /*
     * 整型字符串转换为T枚举类型的转换器
     */
    private static final class StrToEnum<T extends Enum<T> & IBaseEnum> implements Converter<String, T> {
        /*
         * 保存枚举类型
         */
        private final Class<T> enumType;

        /*
         * 构造函数
         */
        private StrToEnum(Class<T> enumType) {
            this.enumType = enumType;
        }


        /*
         * 将给定的字符串转换成对应的枚举类型
         */
        @Override
        public T convert(String source) {
            //接口枚举类型没有传入参数时
            if("undefined".equals(source)){
                return null;
            }

            //字符串类型 将状态码转为枚举类型
            try{
                //成功返回
                T result =  (T) IBaseEnum.fromValue(this.enumType, source);
                if(result != null) {
                    return result;
                }
            }catch (Exception e){
                log.error(e.getMessage(),e);
            }

            //将枚举字符串转为枚举类型
            return (T) IBaseEnum.valueOf(this.enumType, source);
        }
    }
}
