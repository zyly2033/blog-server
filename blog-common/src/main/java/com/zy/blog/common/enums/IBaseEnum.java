package com.zy.blog.common.enums;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.zy.blog.common.serializer.json.JsonEnumDeserializer;
import com.zy.blog.common.serializer.json.JsonEnumSerializer;

/**
 * Author: zy
 * Description: 用户支持将http请求参数中传入的数字转换为对应的枚举值
 * 需要注意的是这里泛型只支持基本数据类型、如Integer、String
 * Date: 2020/5/21
 */
@JsonSerialize(using = JsonEnumSerializer.class)
@JsonDeserialize(using = JsonEnumDeserializer.class)
public interface IBaseEnum<T>{
    /*
     * 与数据库进行映射的值
     */
    T getValue();

    /*
     * 描述信息
     */
    String getDesc();

    /*
     * 将状态码装换为枚举类型
     */
    static <E extends IBaseEnum<T>,T> E fromValue(Class<E> enumClass, T value){
        if(value == null){return null;}
        try {
            E[] enumConstants = enumClass.getEnumConstants();
            for (E e : enumConstants) {
                //这里只所以转为String去比较  是因为JsonEnumDeserializer中调入传入的是String类型
                if (e.getValue().toString().equals(value.toString()))
                    return e;
            }
            return null;
        } catch (Exception ex) {
            throw new IllegalArgumentException("Cannot convert " + value + " to " + enumClass.getSimpleName() + " by code value.", ex);
        }
    }

    /*
     * 将枚举字符串装换为枚举类型
     */
    static <E extends Enum<E>> E valueOf(Class<E> enumClass,String value) {
        return Enum.valueOf(enumClass, value);
    }
}
