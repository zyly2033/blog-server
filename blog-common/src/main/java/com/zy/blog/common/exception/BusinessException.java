package com.zy.blog.common.exception;

import com.zy.blog.common.response.ResponseCode;
import lombok.Data;

/**
 *  @author zy
 *  @since 2020/4/15 0:41
 *  @Description: 业务出错统一处理异常
 */
@Data
public class BusinessException extends RuntimeException {

    /**
     * 状态码
     */
    private final ResponseCode responseCode;

    /**
     * 构造函数
     */
    public BusinessException(ResponseCode responseCode) {
        super(responseCode == null ? ResponseCode.FAIL.getMessage():responseCode.getMessage());
        this.responseCode = responseCode == null ? ResponseCode.FAIL:responseCode;
    }
}
