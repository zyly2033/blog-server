package com.zy.blog.common.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;


/**
 *  @author zy
 *  @since 2020/4/14 23:26
 *  @Description:自动填充功能
 *  https://mp.baomidou.com/guide/auto-fill-metainfo.html
 */
@Slf4j
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {
    @Override
    public void insertFill(MetaObject metaObject) {
        // 起始版本 3.3.0(推荐使用)
        log.info("start insert fill ....");
        //必须手动清空 不然有值的话无法覆盖
        metaObject.setValue("createTime",null);
        this.strictInsertFill(metaObject,"createdAt",LocalDateTime.class,  LocalDateTime.now());
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        // 起始版本 3.3.0(推荐使用)
        log.info("start update fill ....");
        //必须手动清空 不然有值的话无法覆盖
        metaObject.setValue("updateTime",null);
        this.strictUpdateFill(metaObject, "updatedAt", LocalDateTime.class, LocalDateTime.now());
    }
}
