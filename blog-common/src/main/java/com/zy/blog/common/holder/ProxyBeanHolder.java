package com.zy.blog.common.holder;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *  自定义数据结构 用于存放代理信息
 *
 *  @author zy
 *  @since 2020/6/20 15:03
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProxyBeanHolder {
    /**
     * 切面类名称
     */
    private String className;

    /**
     * 代理方法 如：testBefore
     */
    private String methodName;

    /**
     * 通知注解类名称 如：{@link com.zy.blog.common.annotation.Before}
     */
    private String annotationName;
}
