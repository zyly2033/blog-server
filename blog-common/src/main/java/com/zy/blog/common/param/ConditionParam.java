package com.zy.blog.common.param;

import com.zy.blog.common.enums.ConditionEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;


/**
 * Author: zy
 * Description: 高级查询 json格式
 * Date: 2020/5/20
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConditionParam {
    @ApiModelProperty(value = "查询字段",example="createTime")
    private String field;

    @NotNull(message="查询条件不能为空")
    @ApiModelProperty(value = "查询条件",example = "GE")
    private ConditionEnum condition;

    @ApiModelProperty(value = "查询值",example = "2020-02-14")
    private String value;
}
