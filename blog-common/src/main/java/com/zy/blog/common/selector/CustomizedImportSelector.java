package com.zy.blog.common.selector;

import com.zy.blog.common.processor.CustomizedBeanFactoryPostProcessor;
import com.zy.blog.common.processor.CustomizedBeanPostProcessor;
import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;


/**
 *  自定义aop实现,提交给spring容器
 *  ImportSelector 接口有一个实现方法，返回一个字符串类型的数组，里面可以放类名，在@import(ImportSelector.class)的时候，spring会把我们返回方法里面的类全部注册到
 *  BeanDefinitionMap中，继而将对象注册到Spring容器中\
 *
 *  @author zy
 *  @since 2020/6/20 17:16
 */
public class CustomizedImportSelector implements ImportSelector {
    @Override
    public String[] selectImports(AnnotationMetadata annotationMetadata) {
        return new String[]{CustomizedBeanFactoryPostProcessor.class.getName(), CustomizedBeanPostProcessor.class.getName()};
    }
}
