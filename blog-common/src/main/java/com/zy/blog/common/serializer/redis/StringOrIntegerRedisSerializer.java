package com.zy.blog.common.serializer.redis;

import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * Simple {@link String} to {@literal byte[]} (and back) serializer. Converts {@link String Strings}
 * into bytes and vice-versa using the specified charset (by default {@literal UTF-8}).
 * <p>
 * Useful when the interaction with the Redis happens mainly through Strings.
 * <p>
 * Does not perform any {@literal null} conversion since empty strings are valid keys/values.
 *
 * @author Costin Leau
 * @author Christoph Strobl
 * @author Mark Paluch
 */
public class StringOrIntegerRedisSerializer implements RedisSerializer<Object> {

    private final Charset charset;

    /**
     * {@link org.springframework.data.redis.serializer.StringRedisSerializer} to use 7 bit ASCII, a.k.a. ISO646-US, a.k.a. the Basic Latin block of the Unicode
     * character set.
     *
     * @see StandardCharsets#US_ASCII
     * @since 2.1
     */
    public static final org.springframework.data.redis.serializer.StringRedisSerializer US_ASCII = new org.springframework.data.redis.serializer.StringRedisSerializer(StandardCharsets.US_ASCII);

    /**
     * {@link org.springframework.data.redis.serializer.StringRedisSerializer} to use ISO Latin Alphabet No. 1, a.k.a. ISO-LATIN-1.
     *
     * @see StandardCharsets#ISO_8859_1
     * @since 2.1
     */
    public static final org.springframework.data.redis.serializer.StringRedisSerializer ISO_8859_1 = new org.springframework.data.redis.serializer.StringRedisSerializer(StandardCharsets.ISO_8859_1);

    /**
     * {@link org.springframework.data.redis.serializer.StringRedisSerializer} to use 8 bit UCS Transformation Format.
     *
     * @see StandardCharsets#UTF_8
     * @since 2.1
     */
    public static final org.springframework.data.redis.serializer.StringRedisSerializer UTF_8 = new org.springframework.data.redis.serializer.StringRedisSerializer(StandardCharsets.UTF_8);

    /**
     * Creates a new {@link org.springframework.data.redis.serializer.StringRedisSerializer} using {@link StandardCharsets#UTF_8 UTF-8}.
     */
    public StringOrIntegerRedisSerializer() {
        this(StandardCharsets.UTF_8);
    }

    /**
     * Creates a new {@link org.springframework.data.redis.serializer.StringRedisSerializer} using the given {@link Charset} to encode and decode strings.
     *
     * @param charset must not be {@literal null}.
     */
    public StringOrIntegerRedisSerializer(Charset charset) {

        Assert.notNull(charset, "Charset must not be null!");
        this.charset = charset;
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.data.redis.serializer.RedisSerializer#deserialize(byte[])
     */
    @Override
    public String deserialize(@Nullable byte[] bytes) {
        return (bytes == null ? null : new String(bytes, charset));
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.data.redis.serializer.RedisSerializer#serialize(java.lang.Object)
     */
    @Override
    public byte[] serialize(@Nullable Object o) {
        if(o instanceof String){
            return o.toString().getBytes(charset);
        }else{
            return (o == null ? null : String.valueOf(o).getBytes(charset));
        }

    }
}
