package com.zy.blog.dao;

import com.zy.blog.bean.model.BlogRequestPathPrivilegeMapping;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 路径权限关系表 服务类
 * </p>
 *
 * @author zy
 * @since 2020-04-15
 */
public interface IBlogRequestPathPrivilegeMappingService extends IService<BlogRequestPathPrivilegeMapping> {


}
