package com.zy.blog.dao;

import com.zy.blog.bean.model.BlogRequestPath;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 请求路径 服务类
 * </p>
 *
 * @author zy
 * @since 2020-04-15
 */
public interface IBlogRequestPathService extends IService<BlogRequestPath> {

}
