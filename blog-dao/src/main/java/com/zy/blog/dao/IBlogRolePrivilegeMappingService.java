package com.zy.blog.dao;

import com.zy.blog.bean.model.BlogRolePrivilegeMapping;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色权限关系表 服务类
 * </p>
 *
 * @author zy
 * @since 2020-04-15
 */
public interface IBlogRolePrivilegeMappingService extends IService<BlogRolePrivilegeMapping> {

}
