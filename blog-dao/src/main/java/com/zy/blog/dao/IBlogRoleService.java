package com.zy.blog.dao;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zy.blog.bean.model.BlogRole;

/**
 * <p>
 * 用户角色表 服务类
 * </p>
 *
 * @author zy
 * @since 2020-04-15
 */
public interface IBlogRoleService extends IService<BlogRole> {

}
