package com.zy.blog.dao;

import com.zy.blog.bean.model.BlogUserRoleMapping;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户角色关系表 服务类
 * </p>
 *
 * @author zy
 * @since 2020-04-15
 */
public interface IBlogUserRoleMappingService extends IService<BlogUserRoleMapping> {

}
