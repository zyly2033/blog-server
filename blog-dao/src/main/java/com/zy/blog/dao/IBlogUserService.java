package com.zy.blog.dao;

import com.zy.blog.bean.model.BlogUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * InnoDB free: 3072 kB; InnoDB free: 3072 kB 服务类
 * </p>
 *
 * @author zy
 * @since 2020-04-15
 */
public interface IBlogUserService extends IService<BlogUser> {

}
