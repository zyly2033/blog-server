package com.zy.blog.dao.impl;

import com.zy.blog.bean.model.BlogCategory;
import com.zy.blog.dao.mapper.BlogCategoryMapper;
import com.zy.blog.dao.IBlogCategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * InnoDB free: 3072 kB; InnoDB free: 3072 kB 服务实现类
 * </p>
 *
 * @author zy
 * @since 2020-04-15
 */
@Service
public class BlogCategoryServiceImpl extends ServiceImpl<BlogCategoryMapper, BlogCategory> implements IBlogCategoryService {

}
