package com.zy.blog.dao.impl;

import com.zy.blog.bean.model.BlogReply;
import com.zy.blog.dao.mapper.BlogReplyMapper;
import com.zy.blog.dao.IBlogReplyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * InnoDB free: 3072 kB; InnoDB free: 3072 kB 服务实现类
 * </p>
 *
 * @author zy
 * @since 2020-04-15
 */
@Service
public class BlogReplyServiceImpl extends ServiceImpl<BlogReplyMapper, BlogReply> implements IBlogReplyService {

}
