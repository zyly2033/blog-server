package com.zy.blog.dao.impl;

import com.zy.blog.bean.model.BlogRequestPathPrivilegeMapping;
import com.zy.blog.dao.mapper.BlogRequestPathPrivilegeMappingMapper;
import com.zy.blog.dao.IBlogRequestPathPrivilegeMappingService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 路径权限关系表 服务实现类
 * </p>
 *
 * @author zy
 * @since 2020-04-15
 */
@Service
public class BlogRequestPathPrivilegeMappingServiceImpl extends ServiceImpl<BlogRequestPathPrivilegeMappingMapper, BlogRequestPathPrivilegeMapping> implements IBlogRequestPathPrivilegeMappingService {

}
