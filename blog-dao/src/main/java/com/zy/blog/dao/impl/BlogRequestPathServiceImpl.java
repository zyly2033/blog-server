package com.zy.blog.dao.impl;

import com.zy.blog.bean.model.BlogRequestPath;
import com.zy.blog.dao.mapper.BlogRequestPathMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zy.blog.dao.IBlogRequestPathService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 请求路径 服务实现类
 * </p>
 *
 * @author zy
 * @since 2020-04-15
 */
@Service
public class BlogRequestPathServiceImpl extends ServiceImpl<BlogRequestPathMapper, BlogRequestPath> implements IBlogRequestPathService {

}
