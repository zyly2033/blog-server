package com.zy.blog.dao.impl;

import com.zy.blog.bean.model.BlogRolePrivilegeMapping;
import com.zy.blog.dao.mapper.BlogRolePrivilegeMappingMapper;
import com.zy.blog.dao.IBlogRolePrivilegeMappingService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色权限关系表 服务实现类
 * </p>
 *
 * @author zy
 * @since 2020-04-15
 */
@Service
public class BlogRolePrivilegeMappingServiceImpl extends ServiceImpl<BlogRolePrivilegeMappingMapper, BlogRolePrivilegeMapping> implements IBlogRolePrivilegeMappingService {

}
