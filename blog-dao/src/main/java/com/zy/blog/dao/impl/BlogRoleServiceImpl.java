package com.zy.blog.dao.impl;

import com.zy.blog.bean.model.BlogRole;
import com.zy.blog.dao.mapper.BlogRoleMapper;
import com.zy.blog.dao.IBlogRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色表 服务实现类
 * </p>
 *
 * @author zy
 * @since 2020-04-15
 */
@Service
public class BlogRoleServiceImpl extends ServiceImpl<BlogRoleMapper, BlogRole> implements IBlogRoleService {

}
