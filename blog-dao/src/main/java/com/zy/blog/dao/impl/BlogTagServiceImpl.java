package com.zy.blog.dao.impl;

import com.zy.blog.bean.model.BlogTag;
import com.zy.blog.dao.mapper.BlogTagMapper;
import com.zy.blog.dao.IBlogTagService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * InnoDB free: 3072 kB; InnoDB free: 3072 kB 服务实现类
 * </p>
 *
 * @author zy
 * @since 2020-04-15
 */
@Service
public class BlogTagServiceImpl extends ServiceImpl<BlogTagMapper, BlogTag> implements IBlogTagService {

    @Override
    public List<Map<String, Object>> getTagList() {
        return baseMapper.getTagList();
    }
}
