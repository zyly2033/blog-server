package com.zy.blog.dao.impl;

import com.zy.blog.bean.model.BlogUserRoleMapping;
import com.zy.blog.dao.mapper.BlogUserRoleMappingMapper;
import com.zy.blog.dao.IBlogUserRoleMappingService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色关系表 服务实现类
 * </p>
 *
 * @author zy
 * @since 2020-04-15
 */
@Service
public class BlogUserRoleMappingServiceImpl extends ServiceImpl<BlogUserRoleMappingMapper, BlogUserRoleMapping> implements IBlogUserRoleMappingService {

}
