package com.zy.blog.dao.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zy.blog.bean.model.BlogUser;
import com.zy.blog.dao.mapper.BlogUserMapper;
import com.zy.blog.dao.IBlogUserService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * InnoDB free: 3072 kB; InnoDB free: 3072 kB 服务实现类
 * </p>
 *
 * @author zy
 * @since 2020-04-15
 */
@Service
public class BlogUserServiceImpl extends ServiceImpl<BlogUserMapper, BlogUser> implements IBlogUserService {

}
