package com.zy.blog.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zy.blog.bean.model.BlogComment;

/**
 * <p>
 * InnoDB free: 3072 kB; InnoDB free: 3072 kB Mapper 接口
 * </p>
 *
 * @author zy
 * @since 2020-04-15
 */
public interface BlogCommentMapper extends BaseMapper<BlogComment> {

}
