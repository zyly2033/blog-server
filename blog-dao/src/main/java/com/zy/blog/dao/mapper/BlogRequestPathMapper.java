package com.zy.blog.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zy.blog.bean.model.BlogRequestPath;

/**
 * <p>
 * 请求路径 Mapper 接口
 * </p>
 *
 * @author zy
 * @since 2020-04-15
 */
public interface BlogRequestPathMapper extends BaseMapper<BlogRequestPath> {

}
