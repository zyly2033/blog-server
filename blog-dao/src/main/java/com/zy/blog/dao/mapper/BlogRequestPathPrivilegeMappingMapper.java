package com.zy.blog.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zy.blog.bean.model.BlogRequestPathPrivilegeMapping;

/**
 * <p>
 * 路径权限关系表 Mapper 接口
 * </p>
 *
 * @author zy
 * @since 2020-04-15
 */
public interface BlogRequestPathPrivilegeMappingMapper extends BaseMapper<BlogRequestPathPrivilegeMapping> {

}
