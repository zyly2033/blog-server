package com.zy.blog.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zy.blog.bean.model.BlogRolePrivilegeMapping;

/**
 * <p>
 * 角色权限关系表 Mapper 接口
 * </p>
 *
 * @author zy
 * @since 2020-04-15
 */
public interface BlogRolePrivilegeMappingMapper extends BaseMapper<BlogRolePrivilegeMapping> {

}
