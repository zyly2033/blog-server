package com.zy.blog.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zy.blog.bean.model.BlogUserRoleMapping;


/**
 * <p>
 * 用户角色关系表 Mapper 接口
 * </p>
 *
 * @author zy
 * @since 2020-04-15
 */
public interface BlogUserRoleMappingMapper extends BaseMapper<BlogUserRoleMapping> {

}
