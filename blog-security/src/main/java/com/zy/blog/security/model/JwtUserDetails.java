package com.zy.blog.security.model;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.zy.blog.bean.model.BlogUser;
import com.zy.blog.common.util.SpringContextUtil;
import com.zy.blog.bean.model.BlogPrivilege;
import com.zy.blog.dao.IBlogPrivilegeService;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;


import java.util.*;

/**
 * Author: zy
 * Description: 定义满足Spirng Security规则的用户实体类
 * Date: 2020/4/15
 */
public class JwtUserDetails extends BlogUser implements UserDetails {
    /*
     * 权限服务
     */
    private static IBlogPrivilegeService blogPrivilegeService;

    static{
        blogPrivilegeService = SpringContextUtil.getBean(IBlogPrivilegeService.class);
    }

    public JwtUserDetails(BlogUser blogUser){
        BeanUtil.copyProperties(blogUser,this);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        //获取用户权限
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        List<HashMap<String,Object>> privilegeMaps = blogPrivilegeService.getPrivilegeListByUserId(id);
        if(CollUtil.isEmpty(privilegeMaps)) {
            return grantedAuthorities;
        }

        //遍历权限点
        List<BlogPrivilege> blogPrivileges = new ArrayList<>();
        for (Map<String, Object> item : privilegeMaps) {
            blogPrivileges.add(BeanUtil.mapToBean(item, BlogPrivilege.class,false));
        }

        //声明用户授权
        blogPrivileges.forEach(blogPrivilege -> {
            GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(blogPrivilege.getPrivilegeCode());
            grantedAuthorities.add(grantedAuthority);
        });
        return grantedAuthorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
