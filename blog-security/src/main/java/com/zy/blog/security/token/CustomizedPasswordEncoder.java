package com.zy.blog.security.token;

import com.zy.blog.common.util.EncryptUtil;
import org.springframework.security.crypto.password.PasswordEncoder;


/**
 *  @author zy
 *  @since 2020/4/15 21:37
 *  @Description: 自定义加密方式
 */
public class CustomizedPasswordEncoder implements PasswordEncoder {
    @Override
    public String encode(CharSequence charSequence) {
        return EncryptUtil.encryptToBase64((String)charSequence);
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return encodedPassword.equals(EncryptUtil.encryptToBase64((String)rawPassword));
    }

    public String decode(String password){
        return EncryptUtil.decryptFromBase64(password);
    }
}
