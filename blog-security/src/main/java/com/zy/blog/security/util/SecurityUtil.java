package com.zy.blog.security.util;


import com.zy.blog.bean.model.BlogUser;
import com.zy.blog.common.exception.BusinessException;
import com.zy.blog.common.response.ResponseCode;
import lombok.experimental.UtilityClass;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * 安全工具类
 *
 * @author L.cm
 */
@UtilityClass
public class SecurityUtil {
	/**
	 * 获取Authentication
	 */
	public Authentication getAuthentication() {
		return SecurityContextHolder.getContext().getAuthentication();
	}

	/**
	 * 获取系统用户
	 */
	public BlogUser getSysUser() {
		Authentication authentication = getAuthentication();
		Object principal = authentication.getPrincipal();
		if (principal instanceof BlogUser) {
			return (BlogUser) principal;
		}
		throw new BusinessException(ResponseCode.FAIL.setMessage("无法获取用户信息"));
	}
}
