package com.zy.blog.server.annotation;

import org.mybatis.spring.annotation.MapperScan;

import java.lang.annotation.*;


/**
 *  @author zy
 *  @since 2020/4/14 22:23
 *  @Description:配置 MapperScan 注解
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
@MapperScan(basePackages = "com.zy.blog.dao.mapper")
public @interface CoreMapperScan {
}
