package com.zy.blog.server.service;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.zy.blog.common.entity.PageData;
import com.zy.blog.bean.dto.ArticleAddRequestDTO;
import com.zy.blog.bean.dto.ArticleUpdateRequestDTO;
import com.zy.blog.bean.model.BlogArticle;
import com.zy.blog.bean.vo.ArticleVO;

import java.util.List;


/**
 *  @author zy
 *  @since 2020/4/19 19:10
 *  @Description: 文章接口
 */
public interface IArticleService {
    BlogArticle insertArticle(ArticleAddRequestDTO addRequestDTO);
    Boolean deleteArticle(Integer artcleId);
    Boolean deleteArticles(List<Integer> artcleIds);
    BlogArticle updateArticle(ArticleUpdateRequestDTO updateRequestDTO);
    ArticleVO getArticle(Integer artcleId);
    PageData<BlogArticle> getAriticleListByTagName(Long current, Long pageSize, String tagName, Boolean all);
    PageData<ArticleVO> getArticleList(Long current, Long pageSize, Boolean all,String search, SFunction<BlogArticle, ?> column);
}
