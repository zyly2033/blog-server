package com.zy.blog.server.service;


import com.zy.blog.bean.dto.CommentAddRequestDTO;
import com.zy.blog.bean.vo.ArticleVO;

/**
* @Description：文章评论接口
* @author zy
* @since 2020/5/4 11:31
*/
public interface ICommentService {
    ArticleVO insertComment(CommentAddRequestDTO commentAddRequestDTO);
    void deleteComment(Integer commentId);
}
