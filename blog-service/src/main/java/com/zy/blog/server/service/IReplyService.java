package com.zy.blog.server.service;

import com.zy.blog.bean.dto.ReplyAddRequestDTO;
import com.zy.blog.bean.vo.ArticleVO;

/**
 * @Description：文章评论回复接口
 * @author zy
 * @since 2020/5/4 11:31
 */
public interface IReplyService {
    ArticleVO insertReply(ReplyAddRequestDTO replyAddRequestDTO);
    void deleteReply(Integer replyId);
}
