package com.zy.blog.server.service;

import com.zy.blog.bean.vo.TagVO;

import java.util.List;

/**
 * Author: zy
 * Description: 标签接口
 * Date: 2020/4/22
 */
public interface ITagService {
    List<TagVO> getTagList();
}
