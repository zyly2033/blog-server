package com.zy.blog.server.service;


import com.zy.blog.common.entity.PageData;
import com.zy.blog.bean.dto.UserAddRequestDTO;
import com.zy.blog.bean.dto.UserUpdateRequestDTO;
import com.zy.blog.bean.enums.UserTypeEnum;
import com.zy.blog.bean.model.BlogUser;

/**
 *  @author zy
 *  @since 2020/4/14 22:43
 *  @Description: 用户接口
 */
public interface IUserService {
    BlogUser insertUser(UserAddRequestDTO userAddRequestDTO);
    Boolean deleteUser(Integer userId);
    BlogUser updateUser(UserUpdateRequestDTO userUpdateRequestDTO);
    PageData<BlogUser> getUserList(Long current, Long pageSize, Boolean all, String username, UserTypeEnum userType, String createAtStart, String createAtEnd);
}
