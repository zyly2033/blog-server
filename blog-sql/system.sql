/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 80018
 Source Host           : localhost:3306
 Source Schema         : blog

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 66401

 Date: 01/09/2020 10:11:19

 系统级数据：用户与权限   字段数据、操作日志等
*/

use blog;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- 1、 用户信息表
-- ----------------------------
DROP TABLE IF EXISTS t_sys_user;
create TABLE t_sys_user  (
  user_id           varchar(64)     not null                comment '用户id',
  username          varchar(64)     not null                comment '用户账号',
  nick_name         varchar(64)     not null                comment '用户昵称',
  password          varchar(255)    not null                comment '密码',
  email             varchar(64)     default null            comment '邮件',
  phone_number      varchar(11)     null                    comment '手机号码',
  sex               tinyint         default 0               comment '用户性别：0男，1女，2未知',
  notice            tinyint         default 1               comment '邮件通知：0不通知，1通知',
  github            text            null                    comment 'github账号',
  disabled_discuss  tinyint         default 0               comment '禁言：0不禁言，1禁言',
  status            tinyint         default 0               comment '帐号状态（0正常 1停用）',
  login_ip          varchar(64)     null                    comment '最后登录ip',
  login_time        datetime        null                    comment '最后登录时间',
  created_at        datetime        not null                comment '创建时间',
  create_by         varchar(64)     not null                comment '创建者',
  updated_at        datetime        null                    comment '更新时间',
  update_by         varchar(64)     null                    comment '更新者',
  avatar            varchar(255)    null                    comment '头像地址',
  remark            varchar(255)    null                    comment '备注',
  PRIMARY KEY (user_id),
  unique (username)
) comment '用户信息表';

-- ----------------------------
-- 初始化-用户信息表数据
-- ----------------------------
INSERT INTO t_sys_user VALUES (1, 'zy','郑洋','FDyjVY6P87Su04nUfUGQSw==', '18151521911@163.com','', 0, 0, 'Zhengyang564',0, 0, null,null,'2020-04-15 21:52:39', 1,'2020-05-10 18:12:48', 1,null,'');

-- ----------------------------
-- 2、角色信息表
-- ----------------------------
DROP TABLE IF EXISTS t_sys_role;
create TABLE t_sys_role  (
  role_id           varchar(64)     not null                comment '角色id',
  role_name         varchar(64)     not null                comment '角色名称',
  created_at        datetime        not null                comment '创建时间',
  create_by         varchar(64)     not null                comment '创建者',
  updated_at        datetime        null                    comment '更新时间',
  update_by         varchar(64)     null                    comment '更新者',
  remark            varchar(255)    null                    comment '备注',
  PRIMARY KEY (role_id)
) comment '角色信息表';

-- ----------------------------
-- 初始化-角色信息表数据
-- ----------------------------
INSERT INTO t_sys_role VALUES (1, '管理员', '2020-04-15 21:52:39', 1,'2020-05-10 18:12:48', 1,'');

-- ----------------------------
-- 3、菜单权限表
-- ----------------------------
DROP TABLE IF EXISTS t_sys_menu;
create TABLE t_sys_menu  (
  menu_id             varchar(64)       not null              comment '菜单id',
  menu_name           varchar(64)       not null              comment '菜单名称',
  parent_id           varchar(64)       default 0             comment '父菜单id',
  url                 varchar(255)      null                  comment '路由地址',
  menu_type           tinyint           default 0             comment '菜单类型: 0目录, 1菜单,2按钮',
  perms               varchar(255)      null                  comment '权限标识，如：user:list,user:create',
  icon                varchar(255)      null                  comment '菜单图标',
  created_at          datetime          not null              comment '创建时间',
  create_by           varchar(64)       not null              comment '创建者',
  updated_at          datetime          null                  comment '更新时间',
  update_by           varchar(64)       null                  comment '更新者',
  remark              varchar(255)      null                  comment '备注',
  PRIMARY KEY (menu_id)
)comment '菜单权限信息表';

-- ----------------------------
-- 4、用户和角色关联表
-- ----------------------------
DROP TABLE IF EXISTS t_sys_user_role;
create TABLE t_sys_user_role  (
  user_id           varchar(64)      not null    comment '用户id',
  role_id           varchar(64)      not null    comment '角色id',
  primary key(user_id, role_id)
) comment = '用户和角色关联表' ;

-- ----------------------------
-- 初始化-用户和角色关联表数据
-- ----------------------------
INSERT INTO t_sys_user_role VALUES (1, 1);
INSERT INTO t_sys_user_role VALUES (1, 2);
INSERT INTO t_sys_user_role VALUES (1, 3);

-- ----------------------------
-- 5、角色和菜单关联表  角色1-N菜单
-- ----------------------------
drop table if exists t_sys_role_menu;
create table t_sys_role_menu (
  role_id       varchar(64)     not null        comment '角色id',
  menu_id       varchar(64)     not null        comment '菜单id',
  primary key(role_id, menu_id)
)  comment = '角色和菜单关联表';

-- ----------------------------
-- 初始化-角色和菜单关联表数据
-- ----------------------------
insert into t_sys_role_menu values ('1', '1');
insert into t_sys_role_menu values ('1', '2');

-- ----------------------------
-- 6、操作日志记录
-- ----------------------------
drop table if exists t_sys_log;
create table t_sys_log (
  log_id            varchar(64)        not null                   comment '日志id',
  title             varchar(64)        default ''                 comment '模块标题',
  username          varchar(64)        not null                   comment '用户账号',
  ip                varchar(64)        not null                   comment '操作ip地址',
  location          varchar(255)       null                       comment '操作地点',
  operation         varchar(64)        not null                   comment '用户操作',
  method            varchar(255)       null                       comment '请求方法',
  params            varchar(2000)      null                       comment '请求参数',
  result            varchar(2000)      null                       comment '返回参数',
  status            tinyint            default 0                  comment '操作状态：0正常，1异常',
  msg               varchar(2000)      null                       comment '错误消息',
  time              int                null                       comment '执行时间 单位ms',
  create_at         datetime           not null                           comment '操作时间',
  primary key (log_id)
) comment = '操作日志记录';

-- ----------------------------
-- 7、字典类型表
-- ----------------------------
drop table if exists t_sys_dict_type;
create table t_sys_dict_type
(
  dict_type_id        varchar(64)         not null              comment '字典类型id',
  dict_name           varchar(255)        not null              comment '字典名称',
  dict_type           varchar(255)        not null              comment '字典类型',
  created_at          datetime            not null              comment '创建时间',
  create_by           varchar(64)         not null              comment '创建者',
  updated_at          datetime            null                  comment '更新时间',
  update_by           varchar(64)         null                  comment '更新者',
  remark              varchar(255)        null                  comment '备注',
  primary key (dict_type_id),
  unique (dict_type)
) comment = '字典类型表';

insert into t_sys_dict_type values(1,  '用户性别', 'sys_user_sex', sysdate(), '1', null, null, '用户性别列表');

-- ----------------------------
-- 8、字典数据表
-- ----------------------------
drop table if exists t_sys_dict_data;
create table t_sys_dict_data
(
  dict_data_id        varchar(64)           not null                   comment '字典数据id',
  dict_sort           int(4)                default 0                  comment '字典排序',
  dict_label          varchar(64)           not null                   comment '字典标签',
  dict_value          varchar(64)           not null                   comment '字典键值',
  dict_type_id        varchar(64)           not null                   comment '字典类型id',
  is_default          tinyint               default 0                  comment '是否默认：0是，1：否',
  created_at          datetime              not null                   comment '创建时间',
  create_by           varchar(64)           not null                   comment '创建者',
  updated_at          datetime              null                       comment '更新时间',
  update_by           varchar(64)           null                       comment '更新者',
  remark              varchar(255)          null                       comment '备注',
  primary key (dict_data_id)
) engine=innodb auto_increment=100 comment = '字典数据表';

insert into t_sys_dict_data values(1,  1,  '男',       '0',       'sys_user_sex', 0, sysdate(), '1', null, null, '性别男');
insert into t_sys_dict_data values(2,  2,  '女',       '1',       'sys_user_sex', 1, sysdate(), '1', null, null, '性别女');

-- ----------------------------
-- 9、系统访问记录
-- ----------------------------
drop table if exists t_sys_login;
create table t_sys_login (
  login_id       varchar(64)     not null              comment '访问id',
  username       varchar(64)     not null              comment '用户账号',
  ip             varchar(64)     not null              comment '登录ip地址',
  login_location varchar(255)    null                  comment '登录地点',
  browser        varchar(64)     null                  comment '浏览器类型',
  os             varchar(64)     null                  comment '操作系统',
  status         tinyint         default 0             comment '登录状态：0成功，1失败',
  msg            varchar(255)    null                  comment '提示消息',
  login_time     datetime        not null              comment '访问时间',
  primary key (login_id)
) comment = '系统访问记录';